from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^add_new_plan/$', views.add_new_plan, name='add_new_plan'),
    url(r'^allowed_devices/$', views.allowed_devices, name='allowed_devices'),
    url(r'^update_plan/$', views.update_plan, name='update_plan'),
    url(r'^get_devices_by_resource/$', views.get_devices_by_resource, name='get_devices_by_resource'),
    url(r'^get_devices_by_plans/$', views.get_devices_by_plans, name='get_devices_by_plans'),
    url(r'^get_all_plans/$', views.get_all_plans, name = 'get_all_plans'),
    url(r'^get_plan/$', views.get_plan, name='get_plan'),
    url(r'^subscribe/$', views.subscribe, name='subscribe')
]
