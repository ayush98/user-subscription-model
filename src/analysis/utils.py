
from .models import *
from django.utils import timezone
import datetime

resources_list = {
    "api_frwd": ApiAccess,
    "video_stream": VideoStream
}

def get_plan_name_by_vehicle_id(vehicle_id):
    try:
        return UserPlan.objects.get(vehicle_id=vehicle_id, is_cancelled = False).plan_name
    except:
        return None

def get_all_plans():
    try:
        return Plan.objects.all()
    except:
        return None

def get_allowed_devices_by_resource(resource_name):
    table = resources_list[resource_name]
    resources = table.objects.filter(in_use = True)
    vehicle_ids = []
    for res in resources:
        duration = res.expiry_date- timezone.now()
        seconds = duration.total_seconds()
        if seconds > 100:
            vehicle_ids.append(res.vehicle_id)
    return vehicle_ids

def update_user_resources(resource_name, vehicle_id, constraint, value):
    table = resources_list[resource_name]
    device = table.objects.get(vehicle_id=vehicle_id)
    if constraint == 'nviewers':
        device.nviewers = int(value)
        device.save()
    elif constraint == 'recording':
        if value == 'True' or value == 'TRUE' or value == "true":
            device.recoding = True
            device.save()
        else:
            device.recording = False
            device.save()
    elif constraint == 'stream_time':
        device.stream_time = int(value)
        device.save()
    else:
        pass

def get_vehicle_ids_by_resource(resource_name):
    table = resources_list[resource_name]
    devices = table.objects.all()
    vehicle_ids = []
    if resource_name == 'video_stream':
        for device in devices:
            vehicle_ids.append({"vehicle_id":device.vehicle_id, "expiry_date":device.expiry_date,
                                "simultanoeus_viewers":device.nviewers, "recording":device.recording,
                                "stream_time":device.stream_time})
    elif resource_name == 'api_frwd':
        for device in devices:
            vehicle_ids.append({"vehicle_id":device.vehicle_id, "expiry_date":device.expiry_date})
    return vehicle_ids

def get_vehicle_ids_by_plan(plan_name):
    userplans = UserPlan.objects.filter(plan_name=plan_name)
    vehicle_ids = []
    for plan in userplans:
        if plan.vehicle_id not in vehicle_ids:
            vehicle_ids.append(plan.vehicle_id)
    return vehicle_ids

def get_device_by_vehicle_id(vehicle_id):
    try:
        return Device.objects.get(vehicle_id=vehicle_id)
    except:
        return None

def get_all_plans_by_vehicle_id(vehicle_id):
    userplans = UserPlan.objects.filter(vehicle_id=vehicle_id)
    plans = []
    for plan in userplans:
        plans.append(plan.plan_name)
    return plans

def get_new_plan_detail(plan_name):
    try:
        return Plan.objects.get(plan_name=plan_name)
    except:
        return None

def get_plan_by_vehicle_id(vehicle_id):
    try:
        return UserPlan.objects.get(vehicle_id= vehicle_id, is_cancelled=False)
    except:
        return None

def get_resource_by_vehicle_id(resource,vehicle_id):
    try:
        return resource.objects.get(vehicle_id=vehicle_id, in_use=False)
    except:
        return None

def subscribe_to_new_plan(plan_name, vehicle_id, new_plan_details):
    try:
        existing_plan = UserPlan.objects.get(vehicle_id=vehicle_id, is_cancelled = False)
        existing_plan.is_cancelled = True
        existing_plan.save()
    except Exception as e:
        print(e)

    x = timezone.now()
    plan = UserPlan(plan_name=plan_name, vehicle_id=vehicle_id, created_on=x,
                    expiry_date=datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity))
    plan.save()

def subscribe_to_resources(vehicle_id, new_plan_details):
    if new_plan_details.api_frwd is True:
        try:
            plan = ApiAccess.objects.get(vehicle_id=vehicle_id)
        except:
            plan = None

        if plan is None:
            new_plan = ApiAccess(vehicle_id=vehicle_id, in_use = True,
                expiry_date=datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity))
            new_plan.save()
        else:
            plan.expiry_date = datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity)
            plan.in_use = True
            plan.save()
    else:
        try:
            plan = ApiAccess.objects.get(vehicle_id=vehicle_id)
            if plan is not None:
                plan.in_use = False
                plan.expiry_date =datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity)
                plan.save()
        except:
            pass
    if new_plan_details.video_stream is True:
        try:
            plan = VideoStream.objects.get(vehicle_id=vehicle_id)
        except:
            plan = None
        if plan is None:
            new_plan = VideoStream(vehicle_id=vehicle_id, in_use = True, recording=new_plan_details.vs_recording,
               nviewers=new_plan_details.vs_nviewers, stream_time=new_plan_details.vs_stream_time,
                        expiry_date=datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity))
            new_plan.save()
        else:
            plan.expiry_date = datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity)
            plan.in_use = True
            plan.nviewers = new_plan_details.vs_nviewers
            plan.stream_time = new_plan_details.vs_stream_time
            plan.recording = new_plan_details.vs_recording
            plan.save()
    else:
        try:
            plan = VideoStream.objects.get(vehicle_id=vehicle_id)
            plan.expiry_date = datetime.datetime.now() + datetime.timedelta(days=new_plan_details.validity)
            plan.in_use = False
            plan.nviewers = new_plan_details.vs_nviewers
            plan.stream_time = new_plan_details.vs_stream_time
            plan.recording = new_plan_details.vs_recording
            plan.save()
        except:
            pass

def get_all_devices():
    try:
        return Device.objects.filter(is_deleted=False)
    except:
        return None

