# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.response import Response
from .utils import *
# Create your views here.


resources_list = {
    "api_frwd": ApiAccess,
    "video_stream": VideoStream
}


@api_view(http_method_names=['POST'])
@permission_classes(permission_classes=())
def add_new_plan(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        data=request.data
        required_fields = ['plan_name','api_frwd','video_stream','validity','vs_nviewers','vs_recording','vs_stream_time']
        for item in required_fields:
            if item not in data:
                return Response(data={"error": "Field " + item + " must be present" },status=status.HTTP_400_BAD_REQUEST)

        if data['api_frwd'] == 'True':
            api_frwd = True
        else:
            api_frwd = False
        if data['video_stream'] == 'True':
            video_stream = True
        else:
            video_stream = False
        if data['vs_recording'] == 'True':
            recording = True
        else:
            recording = False
        new_plan = Plan(plan_name=data['plan_name'], api_frwd=api_frwd, video_stream=video_stream,
                        vs_nviewers=data['vs_nviewers'], vs_recording=recording, validity= data['validity'],
                        vs_stream_time=data['vs_stream_time'])
        new_plan.save()
        return Response(data={"message":"New Plan is added to the Database"}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)





@api_view(http_method_names=['GET'])
@permission_classes(permission_classes=())
def allowed_devices(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        if 'HTTP_RESOURCENAME' not in request.META:
            return Response(data={"error": "Please enter all the parameters."}, status=status.HTTP_400_BAD_REQUEST)
        resource_name = request.META['HTTP_RESOURCENAME']
        if resource_name not in resources_list:
            return Response(data={"error": "Invalid Resource Name."}, status=status.HTTP_400_BAD_REQUEST)


        vehicle_ids = get_allowed_devices_by_resource(resource_name=resource_name)
        return Response(data={"Vehicle IDs ": vehicle_ids}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)

@api_view(http_method_names=['POST'])
@permission_classes(permission_classes=())
def update_plan(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        data = request.data
        required_fields = ['resource_name', 'vehicle_id', 'constraint', 'value']
        mutable_fields = ['expiry_date']
        for item in required_fields:
            if item not in data:
                return Response(data={"error": "Field " + item + " must be present."},status=status.HTTP_400_BAD_REQUEST)
        if data['constraint'] in mutable_fields:
            return Response(data={"error": "Field " + data['constraint'] + " is mutable."}, status=status.HTTP_400_BAD_REQUEST)

        if data['resource_name'] not in resources_list:
            return Response(data={"error": "Invalid Resource Name."}, status=status.HTTP_400_BAD_REQUEST)


        update_user_resources(data['resource_name'], data['vehicle_id'], data['constraint'], data['value'])

        return Response(data={"message":"Done"}, status=status.HTTP_200_OK)

    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)

@api_view(http_method_names=['GET'])
@permission_classes(permission_classes=())
def get_devices_by_resource(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        if 'HTTP_RESOURCENAME' not in request.META:
            return Response(data={"error": "Please Enter Resource Name."}, status=status.HTTP_400_BAD_REQUEST)
        resource_name = request.META['HTTP_RESOURCENAME']
        if resource_name not in resources_list:
            return Response(data={"error": "Invalid Resource Name."}, status=status.HTTP_400_BAD_REQUEST)

        vehicle_ids = get_vehicle_ids_by_resource(resource_name=resource_name)
        return Response(data={"Vehicle IDs ":vehicle_ids}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)

@api_view(http_method_names=['GET'])
@permission_classes(permission_classes=())
def get_devices_by_plans(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        data = request.data
        if 'HTTP_PLANNAME' not in request.META:
            return Response(data={"error": "Please Enter Plan Name."}, status=status.HTTP_400_BAD_REQUEST)
        plan_name = request.META['HTTP_PLANNAME']
        vehicle_ids = get_vehicle_ids_by_plan(plan_name=plan_name)
        return Response(data={"Vehicle IDs": vehicle_ids}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['GET'])
@permission_classes(permission_classes=())
def get_all_plans(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        if 'HTTP_VEHICLEID' not in request.META:
            return Response(data={"error": "Please Enter vehicle ID."}, status=status.HTTP_400_BAD_REQUEST)
        vehicle_id = request.META['HTTP_VEHICLEID']

        userplans = get_all_plans_by_vehicle_id(vehicle_id)
        return Response(data={"Plan Name":userplans}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['GET'])
@permission_classes(permission_classes=())
def get_plan(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':

        if 'HTTP_VEHICLEID' not in request.META:
            return Response(data={"error": "Please Enter vehicle ID."}, status=status.HTTP_400_BAD_REQUEST)
        vehicle_id = request.META['HTTP_VEHICLEID']
        plan_name = get_plan_name_by_vehicle_id(vehicle_id)
        return Response(data={"Plan Name":plan_name}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['POST'])
@permission_classes(permission_classes=())
def subscribe(request):
    if request.META['HTTP_SERVICE'] == 'dev.flytbase.com':
        data = request.data
        # check for the plan_name & vehicle_id in data
        required_fields = ['plan_name', 'vehicle_id']
        for item in required_fields:
            if item not in data:
                return Response(data={"error": "Field " + item + " must be present" },status=status.HTTP_400_BAD_REQUEST)

        vehicle_id = data['vehicle_id']
        plan_name = data['plan_name']

        try:
            user_existing_plan = get_plan_by_vehicle_id(vehicle_id=vehicle_id)
            user_existing_plan.is_cancelled = False
            user_existing_plan.save()
        except:
            pass
        new_plan_details = get_new_plan_detail(plan_name)
        subscribe_to_new_plan(vehicle_id=vehicle_id, plan_name=plan_name, new_plan_details=new_plan_details)
        subscribe_to_resources(vehicle_id=vehicle_id,new_plan_details=new_plan_details)
        return Response(data={"message":"User is subscribed to "+ plan_name}, status=status.HTTP_200_OK)
    else:
        return Response(data={"User is not allowed in this Area."}, status=status.HTTP_400_BAD_REQUEST)

