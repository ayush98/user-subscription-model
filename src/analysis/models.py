# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Plan(models.Model):
    plan_name = models.CharField(max_length=100)
    api_frwd = models.BooleanField(default=False)
    video_stream = models.BooleanField(default=False)
    validity = models.IntegerField(default=0)
    vs_recording = models.BooleanField(default=False)
    vs_nviewers = models.IntegerField(default=0)
    vs_stream_time = models.IntegerField(default=0)

class UserPlan(models.Model):
    plan_name = models.CharField(max_length=100)
    vehicle_id = models.CharField(max_length=10)
    is_cancelled = models.BooleanField(default=False)
    expiry_date = models.DateTimeField(null=True, blank=True)
    created_on = models.DateTimeField(null=True, blank=True)

class ApiAccess(models.Model):
    vehicle_id = models.CharField(max_length=10)
    expiry_date = models.DateTimeField(null=True, blank=True)
    in_use = models.BooleanField(default=False)

class VideoStream(models.Model):
    vehicle_id = models.CharField(max_length=10)
    expiry_date = models.DateTimeField(null=True, blank=True)
    recording = models.BooleanField(default=False)
    nviewers = models.IntegerField(default=0)
    stream_time = models.IntegerField(default=0)
    in_use = models.BooleanField(default=False)

